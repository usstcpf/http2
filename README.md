# NGINX https
## xxx.crt 转化成xxx.cer 格式
在服务器人员，给你发送的crt证书后，进到证书路径，执行下面语句  openssl x509 -in 你的证书.crt -out 你的证书.cer -outform der 这样你就可以得到cer类型的证书了

## Java证书导入

```
//如果出现keytool 错误: java.io.FileNotFoundException: cacerts (拒绝访问。)，请用管理员身份打开cmd
//载入证书
keytool -import -alias abc -keystore cacerts -file D://abc.cer
//查看证书
keytool -list -keystore cacerts -alias abc
```
## Nginx命令

```
nginx -s reload|reopen|stop|quit  #重新加载配置|重启|停止|退出 nginx
nginx -t   #测试配置是否有语法错误

nginx [-?hvVtq] [-s signal] [-c filename] [-p prefix] [-g directives]

-?,-h           : 打开帮助信息
-v              : 显示版本信息并退出
-V              : 显示版本和配置选项信息，然后退出
-t              : 检测配置文件是否有语法错误，然后退出
-q              : 在检测配置文件期间屏蔽非错误信息
-s signal       : 给一个 nginx 主进程发送信号：stop（停止）, quit（退出）, reopen（重启）, reload（重新加载配置文件）
-p prefix       : 设置前缀路径（默认是：/usr/local/Cellar/nginx/1.2.6/）
-c filename     : 设置配置文件（默认是：/usr/local/etc/nginx/nginx.conf）
-g directives   : 设置配置文件外的全局指令
```


## 请求转发

```
rewrite ^(.*)$ https://$host$1 permanent;
```

# OkHttp
## ALPN
- 应用层协议协商（ALPN）是SSL协议的扩展，有助于更快速地建立HTTPS连接。它与HTTP / 2一起定义，HTTP / 2使用ALPN创建HTTPS连接。由于大多数浏览器仅通过HTTPS实现HTTP / 2，所以任何实现HTTP / 2的服务器或客户端都应该支持ALPN。 OpenJDK在sun.security.ssl包中实现了SSL。 Java 7和Java 8中的当前实现不支持ALPN。使用Java 9，ALPN将成为Java SE标准的一部分，即Java 9将为ALPN提供原生支持。只要Java 9没有准备好，就必须使用一个支持ALPN的外部SSL库。 Jetty项目为sun.security.ssl提供了一个ALPN支持的库。由于SSL实现是Java运行时的一部分，因此Jetty的ALPN JAR不能简单地放入CLASSPATH中，Java必须使用如下参数启动以覆盖Java的启动类路径。

```
-Xbootclasspath/p:<path-to-alpn-boot-VERSION.jar>
```
- OpenJDK版本和相应版本的ALPN对应关系。见表15.1 http://www.eclipse.org/jetty/documentation/current/alpn-chapter.html
- JAR的二进制版本下载地址。http://repo1.maven.org/maven2/org/mortbay/jetty/alpn/alpn-boot/

## 测试http/2的一个例子（2015年）
使用一个TCP连接，测试http/2的多路复用。地址： https://github.com/fstab/http2-examples/tree/master/multiplexing-examples


# 资料
- http/2当前一些实现 https://github.com/http2/http2-spec/wiki/Implementations
- 极客学院http/2讲解 http://wiki.jikexueyuan.com/project/http-2-explained/protocol.html
- http/2 server push http://www.alloyteam.com/2017/01/http2-server-push-research/
- http/2连接建立 http://www.blogjava.net/yongboy/archive/2015/03/18/423570.html
- 使用OkHttp访问自签名证书的Https接口 http://blog.csdn.net/sinat_14849739/article/details/72844947
- spring boot让服务器同时支持http、https http://blog.csdn.net/gary_yan/article/details/77987505
- Spring Boot中启动HTTPS http://blog.csdn.net/RO_wsy/article/details/51319963
- springboot添加https支持 http://blog.csdn.net/u012702547/article/details/53790722
- 快速搭建http/2网站（nginx部署） https://zhuanlan.zhihu.com/p/25935872 http://www.jianshu.com/p/e1170565e9ff